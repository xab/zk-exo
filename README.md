# zk-exo
It uses ansible 2.10 (easier to extract tar.gz file with unarchive module).

Use python3 venv to install ansible

```bash
$ python -m venv ansible-zkexo
$ source zk-exo/bin/activate
(ansible-zkexo) $ pip install -r requirements.txt
```

Inventory file contains id for the cluster

Assuming you have a user on all machine with sudo privileges

This role will install Zookeeper following those steps:

* Create a dedicated user to run Zookeeper
* Check if a folder in /opt exists
* Download Zookeeper if it doesn't (according version in defaults/main.yml)
* Generate Zookeeper config and myID file
* Create a systemd unit file
* Enable the service and start it as dedicated user

## Monitoring
* Using 4 letter word via the command port:
  *  mntr: script must handle only exposed key/value depending on role (leader or not)
